from __future__ import annotations
from src.tape import Tape
from typing import Callable, Literal, Any
from operator import add, mul as mult, floordiv as div, eq, ne, le, ge, lt, gt
from dataclasses import dataclass


class ParseError(Exception):
    pass


def _value_lookup(d: dict, value):
    return next(k for k, v in d.items() if v == value)


def _safe_get(d: dict[str, Any], key: str, msg: str, name: str):
    try:
        return d[key]
    except ValueError:
        raise ParseError(f"{msg}: {name} {key} not in {', '.join(d.keys())}")


def _safe_int(x: str, msg: str, name: str):
    try:
        return int(x)
    except ValueError:
        raise ParseError(f"{msg}: cannot parse {name} {x}")


def sub(a, b) -> int:
    return max(a - b, 0)


def load(_, b) -> int:
    return b


def true(*_) -> bool:
    return True


Indirection = Literal["const", "direct", "indirect"]
PREFIX2IND: dict[str, Indirection] = {
    "": "direct",
    "c-": "const",
    "ind-": "indirect",
}


class ProgramEnd(Exception):
    pass


class Instr:
    @staticmethod
    def parse(line: str) -> Instr:
        cmd, *args = line.lower().split()

        # special cases
        prefix = f"Invalid instruction {line}"
        match cmd:
            case "goto":
                try:
                    (addr,) = args
                except ValueError:
                    raise ParseError(f"{prefix}: should be of format: 'GOTO' <addr>")

                addr = int(addr)
                return IfGoto(true, 0, addr)
            case "if":
                try:
                    (op, arg, goto, addr) = args
                    if goto != "goto":
                        raise ValueError
                except ValueError:
                    raise ParseError(f"Invalid instruction {line}: should be of format: 'IF' <op> <num> 'GOTO' <addr>")

                op = _safe_get(IfGoto.OPS, op, prefix, "comparison")
                arg = _safe_int(arg, prefix, "constant")
                addr = _safe_int(addr, prefix, "address")
                return IfGoto(op, arg, addr)
            case "end":
                try:
                    () = args
                except ValueError:
                    raise ParseError(f"{prefix}: should be of format: 'END'")

                return End()
            case "store":
                try:
                    (addr,) = args
                except ValueError:
                    raise ParseError(f"{prefix}: should be of format: 'STORE' <addr>")

                addr = _safe_int(addr, prefix, "address")
                return Store(PREFIX2IND[""], addr)
            case "ind-store":
                try:
                    (addr,) = args
                except ValueError:
                    raise ParseError(f"{prefix}: should be of format: 'IND-STORE' <addr>")

                addr = _safe_int(addr, prefix, "address")
                return Store(PREFIX2IND["ind-"], addr)

        # arithmetic operations
        i = cmd.find("-") + 1
        ind = _safe_get(PREFIX2IND, cmd[:i], prefix, "indirection")
        op = _safe_get(ArithmOp.OPS, cmd[i:], prefix, "operation")
        arg = _safe_int(args[0], prefix, "argument")
        return ArithmOp(op, ind, arg)

    def __str__(self) -> str:
        raise NotImplementedError

    def exec(self, _: Tape) -> None | int:
        raise NotImplementedError


class End(Instr):
    def __str__(self) -> str:
        return "end"

    def exec(self, _: Tape):
        raise ProgramEnd


@dataclass
class Store(Instr):
    ind: Indirection
    arg: int

    def __post_init__(self):
        if self.ind == "const":
            raise ParseError("There is no constant store")

    def __str__(self) -> str:
        ind = _value_lookup(PREFIX2IND, self.ind)
        return f"{ind}store {self.arg}"

    def exec(self, tape: Tape):
        match self.ind:
            case "const":
                pass
            case "direct":
                tape[self.arg] = tape[0]
            case "indirect":
                tape[tape[self.arg]] = tape[0]


@dataclass
class IfGoto(Instr):
    OPS = {
        "=": eq,
        "==": eq,
        "!=": ne,
        "<=": le,
        ">=": ge,
        "<": lt,
        ">": gt,
        "true": true,
    }

    op: Callable[[int, int], bool]
    arg: int
    addr: int

    def __str__(self) -> str:
        if self.op == IfGoto.OPS["true"]:
            return f"goto {self.addr}"
        else:
            op = _value_lookup(IfGoto.OPS, self.op)
            return f"if {op} {self.arg} goto {self.addr}"

    def exec(self, tape: Tape) -> None | int:
        return self.addr if self.op(tape[0], self.arg) else None


@dataclass
class ArithmOp(Instr):
    OPS = {
        "add": add,
        "sub": sub,
        "mult": mult,
        "div": div,
        "load": load,
    }

    op: Callable[[int, int], int]
    ind: Indirection
    arg: int

    def __str__(self) -> str:
        op = _value_lookup(ArithmOp.OPS, self.op)
        ind = _value_lookup(PREFIX2IND, self.ind)
        return f"{ind}{op} {self.arg}"

    def exec(self, tape: Tape):
        match self.ind:
            case "const":
                opr = self.arg
            case "direct":
                opr = tape[self.arg]
            case "indirect":
                opr = tape[tape[self.arg]]
        tape[0] = self.op(tape[0], opr)


def parse_program(lines: list[str]):
    labels = {}
    program_lines = []
    for line in lines:
        # remove comments
        i = line.find("#")
        if i >= 0:
            line = line[:i]

        # remove whitespace
        line = line.strip()

        # skip empty lines
        if line == "":
            continue

        # check labels
        if line.endswith(":"):
            line = line.removesuffix(":")
            labels[line] = len(program_lines)
        else:
            program_lines.append(line)

    program = []
    for line in program_lines:
        # replace labels
        i = line.find("@")
        if i >= 0:
            label = line[i + 1 :]
            addr = labels[label]
            line = line[:i] + str(addr)

        program.append(Instr.parse(line))
    return program
