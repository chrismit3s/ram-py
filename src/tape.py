from math import log10, ceil


class Tape:
    def __init__(self, input: list[int] = []):
        self.cells = [0] + input

    def __str__(self) -> str:
        return f"[{' '.join(str(x) for x in self.cells)}]"

    def __len__(self) -> int:
        return len(self.cells)

    def __getitem__(self, index: int) -> int:
        self.ensure_size(index + 1)
        return self.cells[index]

    def __setitem__(self, index: int, value: int):
        self.ensure_size(index + 1)
        self.cells[index] = value

    def lines(self) -> str:
        idigits = ceil(log10(max(len(self), 1)))
        xdigits = ceil(log10(max((*self.cells, 1))))
        lines = []
        for i, x in enumerate(self.cells):
            lines.append(f"{i:{idigits}}: {x:{xdigits}}")
        return "\n".join(lines)

    def ensure_size(self, size: int):
        self.cells += [0 for _ in range(size - len(self))]

    def compress(self):
        trailing_zeros = 0
        for x in self.cells[::-1]:
            if x == 0:
                trailing_zeros += 1
            else:
                break
        if trailing_zeros > 0:
            self.cells = self.cells[:-trailing_zeros]
