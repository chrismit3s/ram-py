from src.instr import parse_program, ParseError
from src.ram import RAM
from sys import argv, stderr

if __name__ == "__main__":
    if len(argv) < 2:
        print(f"USAGE: {argv[0]} <file> <input...>", file=stderr)

    try:
        with open(argv[1]) as f:
            program = parse_program(f.readlines())
    except ParseError as e:
        print(f"parse: {e}", file=stderr)
        exit(1)

    input = [int(x) for x in argv[2:]]

    print("Program:")
    ram = RAM(program, input)
    i = ram.run()
    print(f"... took {i} steps")

    print("")
    print("Tape:")
    ram.tape.compress()
    print(ram.tape.lines())
