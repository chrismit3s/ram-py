from contextlib import suppress
from math import log10, ceil
from src.instr import Instr, ProgramEnd
from src.tape import Tape


class RAM:
    def __init__(self, program: list[Instr], input: list[int]):
        self.program = program
        self.tape = Tape(input)
        self.pc = 0

    def step(self):
        jmp = self.program[self.pc].exec(self.tape)
        self.pc = (self.pc + 1) if jmp is None else jmp

    def run(self):
        instrlen = max(len(str(instr)) for instr in self.program)
        pcdigits = ceil(log10(len(self.program)))
        i = 0
        with suppress(ProgramEnd):
            while True:
                print(f"{self.pc:{pcdigits}}: {str(self.program[self.pc]):{instrlen}} {self.tape}")
                self.step()
                i += 1
        return i
