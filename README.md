# RAM
A RAM implementation in python.

## Overview
* run your program with `python -m src <program> "<input...>"`
  * `<program>` is the path to a file containing instructions
  * `<input...>` is a sequence of natural numbers that will be stored in c(1), c(2), ...


    # everything after a hashtag is a comment
    LABEL:  # create labels with <label>: (the next instruction has to be on the next line)
    C-ADD 1
    IF < 10 GOTO @LABEL  # reference labels with @<label>
    STORE 1  # for a list of instructions see below
    END

## Instructions
### Arithmetic
* Direct
  * `ADD x`: c(0) := c(0) + c(x)
  * `SUB x`: c(0) := max(c(0) - c(x), 0)
  * `MULT x`: c(0) := c(0) * c(x)
  * `DIV x`: c(0) := floor(c(0) / c(x))
* Indirect
  * `IND-ADD x`: c(0) := c(0) + c(c(x))
  * `IND-SUB x`: c(0) := max(c(0) - c(c(x)), 0)
  * `IND-MULT x`: c(0) := c(0) * c(c(x))
  * `IND-DIV x`: c(0) := floor(c(0) / c(c(x)))
* Constant
  * `C-ADD x`: c(0) := c(0) + x
  * `C-SUB x`: c(0) := max(c(0) - x, 0)
  * `C-MULT x`: c(0) := c(0) * x
  * `C-DIV x`: c(0) := floor(c(0) / x)

### Memory
* Load
  * `LOAD x`: c(0) := c(x)
  * `IND-LOAD x`: c(0) := c(c(x))
  * `C-LOAD x`: c(0) := x
* Store
  * `STORE x`: c(x) := c(0)
  * `IND-STORE x`: c(c(x)) := c(0)

### Control
* `END` halts executions
* `GOTO x` jumps to instruction x (to use a label here use `GOTO @LABEL`)
* `IF ? x GOTO y` jumps to instruction y if `c(0) ? x` is true (`?` is one of `==`, `!=`, `<=`, `>=`, `<`, `>`)
* `LABEL:` defines a label
* `@LABEL` references a label

## Examples
* `examples/count.ram`

    LOAD 1
    LOOP:
    STORE 1
    IND-STORE 1
    C-SUB 1
    IF > 0 GOTO @LOOP
    END
